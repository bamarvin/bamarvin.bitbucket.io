'''@file My_LED_patterns.py
Code: https://bitbucket.org/bamarvin/doxygen-html-output/src/master/My_LED_patterns.py
Image: https://bitbucket.org/bamarvin/doxygen-html-output/src/master/State-transition_diagram_LED_patterns.png
Video: https://drive.google.com/file/d/1NbxpfmuBlS49P4djHV9FmGel4LwIHOBi/view?usp=sharing
@details    Implements a finite state machine, shown below, that allows a
Nucleo user to interact with their Nucleo by pressing the B1 button to toggle 
through 3 different LED pulse patterns.
@author     Brooke Marvin
@date       02/03/2021
@copyright  License Info Here
'''

# imports 
import utime
import pyb
import math

# button
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
# LED
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)


# define the action of pressing the button
def onButtonpress(IRQ_src):
    '''@brief Commands the Nucleo to toggle through the LED pulse patterns
       @details The LED in use is pin A5
       @param IQR_src The command to activate pin C13
    '''
    global Button_pushed
    Button_pushed = True

# main program
if __name__ == "__main__":
    # a greeting for the user
    print('Welcome. Allow me to instruct you on how to use your Nucleo. '
          'First off, please press the B1 button to interact '
              'with the Nucleo and cycle through LED patterns.')
    
    utime.sleep_ms(5000)
    
    # defined variables and proper starting points for the following code
    Button_pushed = False 
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                            pull=pyb.Pin.PULL_NONE, callback=onButtonpress)
    
    tim2 = pyb.Timer(2, freq = 20000)
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    
    state = 0 # Initial state 
    last_pattern = 0
    start_time_set = False
 
    # while loop
    while True:
        try:
            if state==0:
                t2ch1.pulse_width_percent(0) # LED turn off
                # run state 0 (init) code
                print('Please select pattern')
                # if button is pressed, transition to different states
                if Button_pushed:
                    
                    if last_pattern == 0:
                        state = 1                  
                    if last_pattern == 1:
                        state = 2
                    if last_pattern == 2:
                        state = 3
                    if last_pattern == 3:
                        state = 1
                    
                    Button_pushed = False
                    start_time_set == False
                    
            elif state==1:
                # run state 1
                print('Blinking pattern selected')
                last_pattern = 1
                
                if start_time_set == False:
                    pattern_start_time = utime.ticks_ms()
                    blinking_time = utime.ticks_ms()
                    start_time_set = True
                    
                if utime.ticks_diff(utime.ticks_ms(), pattern_start_time) > 10000:
                    state = 0
                    t2ch1.pulse_width_percent(0) # LED turn off
                    start_time_set = False
                
                # blinking pattern
                if utime.ticks_diff(utime.ticks_ms(), blinking_time) % 1000 < 500:
                    t2ch1.pulse_width_percent(100) # LED turn on
                elif utime.ticks_diff(utime.ticks_ms(), blinking_time) >= 500: 
                    t2ch1.pulse_width_percent(0) # LED turn off
                 

                # if button is pressed, stop LED pattern and transition to S2
                if Button_pushed:
                    t2ch1.pulse_width_percent(0) # LED turn off
                    pattern_start_time = 0
                    state = 2 
                    Button_pushed = False
                    start_time_set = False
                    
            elif state==2:
                # run state 2
                print('Fade pattern selected')
                last_pattern = 2
                
                if start_time_set == False:
                    pattern_start_time = utime.ticks_ms()
                    fade_time = utime.ticks_ms()
                    start_time_set = True
                    
                if utime.ticks_diff(utime.ticks_ms(), pattern_start_time) > 10000:
                    state = 0
                    t2ch1.pulse_width_percent(0) # LED turn off
                    start_time_set = False
                    
                # fade pattern
                if utime.ticks_diff(utime.ticks_ms(), fade_time) % 10000 <= 10000:
                    fade = 1000*(.05*math.sin((utime.ticks_ms() / 1000) /1.6)+.05)
                    t2ch1.pulse_width_percent(fade)


                # if button is pressed, stop LED pattern and transition to S3
                if Button_pushed:
                    t2ch1.pulse_width_percent(0) # LED turn off
                    pattern_start_time = 0                    
                    state = 3 
                    Button_pushed = False
                    start_time_set = False

                    
            elif state==3:
                # run state 3
                print('Saw tooth pattern selected')
                last_pattern = 3

                if start_time_set == False:
                    pattern_start_time = utime.ticks_ms()
                    build_up_time = utime.ticks_ms()
                    start_time_set = True
                
                    
                if utime.ticks_diff(utime.ticks_ms(), pattern_start_time) > 10000:
                    state = 0
                    t2ch1.pulse_width_percent(0) # LED turn off
                    start_time_set = False
                    
                # if start_time_set == True:
                    # print(pattern_start_time/ 1000)
     
                # saw tooth pattern
                if utime.ticks_diff(utime.ticks_ms(), build_up_time) % 1000 <= 1000:
                    duty = 100*((utime.ticks_ms() / 1000) % 1)
                    t2ch1.pulse_width_percent(duty)


                # if button is pressed, stop LED pattern and transition to S1
                if Button_pushed:
                    t2ch1.pulse_width_percent(0) # LED turn off
                    pattern_start_time = 0                    
                    state = 1 # Updating state for next iteration
                    Button_pushed = False
                    start_time_set = False

                    
            else:
                pass
            # code to run if state number is invalid
            # program should ideally never reach here
            

        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the 
            # while(True) loop when desired
            print('Ctrl-c has been pressed. Program ending.')
            break
    
    # Program de-initialization goes here
