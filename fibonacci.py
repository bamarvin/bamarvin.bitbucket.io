'''@file fibonacci.py
Code: https://bitbucket.org/bamarvin/me30502lab/src/master/Lab%201/fibonacci.py There must be a docstring at the beginning of a Python source file
with an \@file [filename] tag in it!
'''
print ('Welcome. Allow me to assist you as we calculate Fibonacci numbers. PS- Press Ctrl-C to exit at any time.')

def fib(idx):
    
    '''
@brief     This function calculates a Fibonacci number at a specific index.
@param idx An integer specifying the index of the desired
            Fibonacci number
'''

    if idx == 0:
        seed = 0
    elif idx ==1:
        seed = 1
    elif idx > 1:
        seed_minus_2 = 0
        seed_minus_1 = 1
        
        for X in range(2, idx+1):
            seed = seed_minus_2 + seed_minus_1
            seed_minus_2 = seed_minus_1
            seed_minus_1 = seed
        return seed
               
if __name__ =='__main__':
    # Replace the following statement with the user interface code
    # that will allow testing of your Fibonnaci function. Any code
    # within the if __name__ == '__main__' block will only run when
    # the script is executed as a standalone program. If the script
    # is imported as a module the code block will not run.
    while True:
        try:
            my_string = input('Please input a positive integer (aka your index): ')
            if "-" in my_string:
                print('I said positive')
            elif my_string.isdigit():
                idx = int(my_string)
                print ('Fibonacci number at ' 
                     'index {:} is {:}.'.format(idx, fib(idx)))
            else:
                print('You had one job --__-- Please input a positive integer')
        
        except KeyboardInterrupt:
                break

    print('See ya later')
    
            # Goals in the future:
                    #Try to allow neg. integers to be computed as well. Should I put two parameters in the calculator?



