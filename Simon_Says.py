'''@file Simon_Says.py
Code: https://bitbucket.org/bamarvin/me30502lab/src/master/lab%203/Simon_Says.py
Image: https://bitbucket.org/bamarvin/me30502lab/src/master/ME%20Lab%200x03%20FSMD.png
Video: https://drive.google.com/file/d/1JZVQNTRIfZZXz7RZsBDaPgZFhi8kjz50/view?usp=sharing
@details    Implements a finite state machine that allows the Nucleo to play Simon Says with the user. During this game, the Nucloe will display LED patterns that the user must then replicate by toggling the B1 button.
@author     Brooke Marvin
@date       02/20/2021
@copyright  License Info Here
'''


# imports 
import utime
import pyb
import random
#import micropython

# button
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
# LED
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)


# define the action of pressing the button
#def onButtonpress(IRQ_src):
    #'''@brief Commands the Nucleo to acknowledge that the button is being pressed
      # @details The LED in use is pin A5
       #@param IQR_src The command to activate pin C13
    #'''
    #global Button_pushed
    #Button_pushed = True
    
#def onButtonrelease(IRQ_src):
    #'''@brief Commands the Nucleo to acknowledge that the button is being released
       #@details The LED in use is pin A5
      # @param IQR_src The command to activate pin C13
    #'''
   # global Button_released
    #Button_released = True
    
    
def Button(IRQ_src):
    '''
    @brief Commands the Nucleo to acknowledge that the button is being pressed
    @details The LED in use is pin A5
    @param IQR_src The command to activate pin C13 
    '''
    print('B1 Button pressed')
    
    #global pushed_flag 
    #pushed_flag = False
    
    pinC13.value() ##Should this be here?

    
    if pinC13.value():
        global Button_released
        Button_released = True
        print('light off')

    
    else:
        global Button_pushed
        Button_pushed = True
        print('light activated')



# individual 'parts' that make up the patterns    
def dot():
    # if button is pressed for a short time

        dot_start_time = utime.ticks_ms() #starting from now, keep track of how long the button is pushed
               
        while utime.ticks_diff(utime.ticks_ms(), dot_start_time) < 500:
            t2ch1.pulse_width_percent(100) # LED turn on
            print('.')

def space():
    # if button is pressed for a short time

        space_start_time = utime.ticks_ms() #starting from now, keep track of how long the button is pushed
               
        while utime.ticks_diff(utime.ticks_ms(), space_start_time) < 2000:
            t2ch1.pulse_width_percent(0) # LED turn on
            print(' ')
    
def dash():
    # if button is pressed for a long time

        dash_start_time = utime.ticks_ms() #starting from now, keep track of how long the button is pushed
        
        while utime.ticks_diff(utime.ticks_ms(), dash_start_time) % 6000 <= 3000: 
            t2ch1.pulse_width_percent(100) # LED stay on
            print('-')

   
    
def my_pattern(): # LED patterns used for Simon Says 
    decision = []    

    A = "['.', '.', '.', '.']" 
    B = "['-', '-', '-', '-']"
    C = "['.', '-', '.', '-']"
    D = "['-', '.', '-', '.']"
    pattern = [A, B, C, D] # pattern list
    
    decision = random.choice(pattern) # Choose a random pattern
    
    if decision == A:
        myCode = ['.', '.', '.', '.']
        dot()
        space()
        dot()
        space()
        dot()
        space()
        dot()
        t2ch1.pulse_width_percent(0)
    elif decision == B:
        myCode = ['-', '-', '-', '-']
        dash()
        space()
        dash()
        space()
        dash()
        space()
        dash()
        t2ch1.pulse_width_percent(0)
    elif decision == C:
        myCode = ['.', '-', '.', '-']
        dot()
        space()
        dash()
        space()
        dot()
        space()
        dash()
        t2ch1.pulse_width_percent(0)
    elif decision == D:
        myCode = ['-', '.', '-', '.']
        dash()
        space()
        dot()
        space()
        dash()
        space()
        dot()
        t2ch1.pulse_width_percent(0)
        
        

    #print(decision)
    #print(myCode)
    #return decision # This will return the pattern once EVER! (Later becomces known as 'myCombo' in my code)
    return myCode
    
def add_to_pattern(input_pattern, count, released_time, pushed_flag, buttpush): ## I am trying to figure out how to let the game player make a code
                    ## Pattern stored in input_pattern = []
    
    global Button_pushed
    global Button_released
    
    
    while True:
      if Button_pushed:
          print('help me i dying')

          t2ch1.pulse_width_percent(100) # LED turn on
          #count = count + 1
          pushed_flag = True        
          #pushed_time = 0
          #released_time = 0

          global pushed_time
          #if pushed_time == 0:
              
          pushed_time = utime.ticks_ms() #starting from now, keep track of how long the button is pushed
          print(pushed_time)
          
          global Button_pushed
          global Button_released
          Button_pushed = False
          Button_released = False
          
          return pushed_flag


      elif Button_released:
          #print('T-T')
          global pushed_time

          t2ch1.pulse_width_percent(0) # LED turn off

          if pushed_flag == True:
              print('the Truth will set you free')
              print(utime.ticks_diff(utime.ticks_ms(), pushed_time))

              if utime.ticks_diff(utime.ticks_ms(), pushed_time) < 2000:
                  #t2ch1.pulse_width_percent(100) # LED turn on
                  input_pattern.append('.') # Attaches what is in the (' ') to the end of a string          
                  print('.')
                  pushed_flag = False


              #elif utime.ticks_diff(utime.ticks_ms(), pushed_time) % 10000 >= 2000:
              else:
                  #t2ch1.pulse_width_percent(100) # LED stay on
                  input_pattern.append('-')            
                  print('-')
                  pushed_flag = False

              print(input_pattern)


          #count = count + 1

          #print('counting')

          return pushed_flag
          #print(count)
        


# main program
if __name__ == "__main__":
    # a greeting for the user
    print("Welcome. Let's play Simon Says.")
    
    utime.sleep_ms(3000)
    
    print("In this game, try your best to copy my LED patterns.")
    
    utime.sleep_ms(3000)
    
    print("Click the B1 button to produce a short blink (a dot; '.'). Hold the B1 button for more than 2 seconds to produce a long blink (a dash; '-').")
    
    utime.sleep_ms(7000)
    
    # interupt 1
    #Button_pushed = False 
    #ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                            #pull=pyb.Pin.PULL_NONE, callback=onButtonpress)
    
    # interrupt 2
    #Button_released = False 
    #ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_RISING,
                            #pull=pyb.Pin.PULL_NONE, callback=onButtonrelease)
    
    # interrupt 3 for rising and falling; button_push and button_release, respectively
    global Button_pushed
    global Button_released
    global pushed_time
    
    Button_pushed = False 
    Button_released = True
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_RISING_FALLING,
                            pull=pyb.Pin.PULL_NONE, callback=Button)
    
    # defined variables and proper starting points for the following code
    tim2 = pyb.Timer(2, freq = 20000)
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    
    state = 0 # Initial state 
    input_pattern = []
    myCombo = []
    count = 0
    points = 0
    pushed_time = 0
    released_time = 0
    dot_start_time = 0
    space_start_time = 0
    dash_start_time = 0
    
    pushed_flag = False
 
    # while loop
    while True:
        try:
            if state == 0:
                t2ch1.pulse_width_percent(0) # LED turn off
                # run state 0 (init) code
                print('Ready? Okay! Please press the B1 button to start.')
                
                while True:
                  if Button_pushed: ## I feel like I need a different function here... not if Button_pushed...

                      pushed_time = 0
                      released_time = 0

                      #my_string = input('Follow my lead.') ## my_string does not work here... because the input is done through the Nucleo
                      print('Follow my lead')

                      utime.sleep_ms(2000)

                      state = 1

                      Button_pushed = False 
                      Button_released = True
                      
                      break
                    
                    
            if state == 1:
                
                myCombo = my_pattern()
                
                
                print(myCombo)
                #print(len(myCombo))
                
                print('Your turn in 3, 2, 1')
                utime.sleep_ms(3000)
                
                state = 2
                    
            if state == 2:
                #count = 0
 
                print('forming your pattern')
                
                #while count <= 3: # Current myCombo length is 3 units
                while len(input_pattern) < 4:
                        pushed_flag = add_to_pattern(input_pattern, count, released_time, pushed_flag, Button_pushed)
                    
                        #print(input_pattern)
                        
                        #count = count + 1 
                        
                        #Button_pushed = False
                        #Button_released = True
                        
                if len(input_pattern) == 4: 
                    print('This is your result: ')
                    print(input_pattern)
                    print('This is the correct pattern: ')
                    print(myCombo)                     
                    #break
                    state = 3
                    
           
            if state == 3:
                print('calculating results...')
                utime.sleep_ms(2000)
                if input_pattern == myCombo:
                
                    print('Well done!')
                    utime.sleep_ms(3000)
                    points = points + 1
                    print(points)                        
                    state = 4
                
                elif input_pattern != myCombo:
                
                    print ("Simon didn't say that. Better luck next time.")
                    utime.sleep_ms(2000)
                    state = 4
                


            if state == 4:
                print('Want to play again? Press the B1 button to continue, otherwise, press press ctrl-c to leave.')
                input_pattern = []
                
                while True:
                    if Button_pushed:
                        state = 0
                        break
            
            #else:
                #pass
            # code to run if state number is invalid
            # program should ideally never reach here
            

        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the 
            # while(True) loop when desired
            print('Ctrl-c has been pressed. Program ending.')
            t2ch1.pulse_width_percent(0) # LED turn off
            break
    
    # Program de-initialization goes here
