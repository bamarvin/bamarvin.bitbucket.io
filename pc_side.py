'''@file pc_side.py
Code: https://bitbucket.org/bamarvin/me30502lab/src/master/Final%20Project/pc_side.py

@details    This code establishes a handshake between a PC and a Nucleo. Pressing 'g' tells the Nucleo to start collecting data for 30 seconds. Pressing 's' tells the Nucleo to stop collecting data. The data that the Nucleo collects will be sent back to the PC. The PC will then plot this data. Furthermore, all collected values will be uploaded to a .csv file. This is the PC side of the code (i.e. this file will be run on the PC in Spyder). 
@author     Brooke Marvin
@date       03/18/2021
@copyright  License Info Here
'''

# PC imports
from matplotlib import pyplot 
from array import array
from math import exp, sin, pi 
import numpy as np

import serial # From example:
ser = serial.Serial(port='COM4',baudrate=115273,timeout=35)

def sendChar(): # Allows the PC to interact with the Nucleo (i.e. I can click keyboard buttons and the Nucleo will register them)
    inv = input('Give me a character: ')
    ser.write(str(inv).encode('ascii')) # PC is writing to the nucleo
    myval = ser.readline().decode('ascii') # PC decodes and associates keyboard keys with number values
    return myval

#for n in range(1):
    #print(sendChar())
    

if __name__ =='__main__':
    
    state = 0
    time = 0

    
    times = array('f', 120*[0]) # Data points on plot
    values = array('f', 120*[0])
    n = 0

    
    myval = '' 
    start_time_set = False
    out_string = ''
    data_collection_time = ''
    
    x = 0
    

    while True:
        try:
            if state == 0:
                print('state 0')
                my_string = input('Press g to start collecting data. Press s to stop collecting data. Press ctrl-c to end the program at any time. Now, please press g to continue: ')

                dateString = sendChar()
                    
              
                if "g" in my_string:
                    print('Collecting data')
                    pyplot.figure()
                    pyplot.xlabel('Time')
                    pyplot.ylabel('Data')
                    pyplot.xlim([-2, 32])
                    pyplot.ylim([-1, 1])
                    start_time_set == False
                    state = 1   
                if "s" in my_string:
                    print('Data collection has not started')
                    state = 0
                    

            
            if state == 1:
                print('state 1')
                if start_time_set == False:
                    print('Please wait as the data is transferred.')
                    

                    
                    for x in range(120):
                        
                        dataString = ser.readline().decode('ascii') #Returns a decoded list
                        #strippedString = dataString.strip()
                        #splitStrings = dataString.split(',')
                        #print(dataString)
                        
                        ###Need Loop to strip and split everything###
                        # As another example, build a single line string, strip special characters,
                        # split on the commas, then float the entries
                        num1 = 1
                        num2 = 2

                        # Generate string (you will read this with ser.read() in your PC script)
                        myLineString = dataString.format(num1, num2)
                        #print(myLineString)
                        
                        # Remove line endings
                        myStrippedString = myLineString.strip()
                        
                        # split on the commas
                        mySplitStrings = myStrippedString.split(',')
                        #print(mySplitStrings)
                        
                        
                        # Convert entries to numbers from strings
                        myNum1 = float(mySplitStrings[0])
                        #print(myNum1) #times
                        myNum2 = float(mySplitStrings[1])
                        #print(myNum2) #values
                        print(myNum1,myNum2)
                        
                        times[x] = myNum1 #Fills array with outputs
                        values[x] = myNum2 #Filles array with outputs
                        

                    pyplot.plot(times, values)
                    

                    

                    if "s" in my_string:
                        print('Data collection stopped')
                        myval = ''
                        state = 0

                        
                    
                if out_string != 0:
                    print('All data has been collected')
                    print('Come back again if you ever want to collect more data!') # The program is only letting me collect data once for some reason...?
                    
                    import csv
                    with open('mycsv.csv', 'w', newline='') as f:
                        fieldnames = ['timestamps', 'data readings']
                        thewriter = csv.DictWriter(f, fieldnames=fieldnames)
    
                        thewriter.writeheader()
                        for x in range(120):
                            thewriter.writerow({'timestamps' : times[x], 'data readings' : values[x]})
                    
                    mytime = 0
                    data_collection_time = 0
                    myval = ''

                    break
              
        
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the 
            # while(True) loop when desired
            print('Ctrl-c has been pressed. Program ending.')
            ser.close()
            break
    
    # Program de-initialization goes here
            
            
            
            
            
            
            
            
            
            
            
            