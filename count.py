'''@file count.py
Code: https://bitbucket.org/bamarvin/me30502lab/src/master/Final%20Project/count.py

@details    As the code is now, you can manually spin the motors and receive count readings labeled "CW" and "CCW." Further coding would have allowed a user to press 'z' to zero the encoder position, 'p' to print the encoder position, 'd' to print the encoder delta, 'g' to collect encoder data for 30 seconds, and 's' to end data collection.
@author     Brooke Marvin
@date       03/18/2021
@copyright  License Info Here
'''

import pyb
import utime
from pyb import Timer, Pin, delay

#class encoder(object, self):

    #def __init__(self):
           
    #def update(self):
        #'''
        #@brief Updates position
        #@details The position must be greater than -Period/2 but less than Period/2
        #'''

    
    #def get_position(self):
    
    #def set_position(self):
    
    #def get_delta(self):
       
    #def timer.counter(self):
        


# Encoder
TIM4 = Timer(4, period=0xFFFF, prescaler=0)
TIM4.channel(1, mode=Timer.ENC_AB, pin=Pin.cpu.B6)
TIM4.channel(2, mode=Timer.ENC_AB, pin=Pin.cpu.B7)

#EN = Pin(Pin.cpu.A15, mode=Pin.OUT_PP, value=1)
if __name__ =='__main__':
    
    state = 0

    while True:
        try:
            if state==0:
                #print('state 0')

                print("CCW: " + str(TIM4.counter()))
                
                print("CW: " + str(TIM4.counter()))
                
                state = 0
                                
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the 
            # while(True) loop when desired
            print('Ctrl-c has been pressed. Program ending.')
            #ser.close()
            break
    
    # Program de-initialization goes here
            
            
            
            
            
            
            
            
            
            
            
            
        
        