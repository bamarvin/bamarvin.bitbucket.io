## @file encoder.py
#  Brief doc for encoder.py
#
#  Detailed doc for encoder.py 
#
#  @author Brooke Marvin
#
#  @copyright License Info
#
#  @date March 18, 2021
#
#  @package encoder
#  Brief doc for the encoder module
#
#  Detailed doc for the encoder module
#
#  @author Brooke Marvin
#
#  @copyright License Info
#
#  @date March 18, 2021

import pyb

## An encoder driver object
#
#  This code establishes a handshake between a PC and a Nucleo. Pressing 'g' tells the Nucleo to start collecting data for 30 seconds. Pressing 's' tells the Nucleo to stop collecting data. The data that the Nucleo collects will be sent back to the PC. The PC will then plot this data. Furthermore, all collected values will be uploaded to a .csv file. 
#  @author Brooke Marvin
#  @copyright License Info
#  @date March 18, 2021
class Encoder:

	## Constructor for encoder driver
	#
	#  Detailed info on encoder driver constructor
	def __init__(self):
		pass

	## Gets the encoder's position
	#
	#  Detailed info on encoder get_position method
	def get_position(self):
		pass

	## Zeros out the encoder
	#
	#  Detailed info on encoder zero function
	def zero(self):
		pass