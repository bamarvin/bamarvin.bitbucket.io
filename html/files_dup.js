var files_dup =
[
    [ "count.py", "count_8py.html", "count_8py" ],
    [ "encoder.py", "encoder_8py.html", [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "fibonacci.py", "fibonacci_8py.html", "fibonacci_8py" ],
    [ "Implementing_FSMs.py", "Implementing__FSMs_8py.html", "Implementing__FSMs_8py" ],
    [ "motor.py", "motor_8py.html", [
      [ "Motor", "classmotor_1_1Motor.html", "classmotor_1_1Motor" ]
    ] ],
    [ "My_LED_patterns.py", "My__LED__patterns_8py.html", "My__LED__patterns_8py" ],
    [ "nucleo_side.py", "nucleo__side_8py.html", "nucleo__side_8py" ],
    [ "pc_side.py", "pc__side_8py.html", "pc__side_8py" ],
    [ "Simon_Says.py", "Simon__Says_8py.html", "Simon__Says_8py" ]
];