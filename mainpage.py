## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This project addresses encoders and motors. The following files of code will allow a user to establish bidirectional communication between a Nucleo and PC. This allows the PC to control the Nucleo's actions. 
#  The Encoder Driver code will have the Nucleo collecting data and sending it back to the PC so that the PC may generate a plot of the collected data. 
#  The Motor Driver code will allow the PC to print the count of a motor, that is connected to the Nucleo, when it is manually turned.
#  Further coding would have allowed the PC to position the motors.
#
#  @section sec_mot Motor Driver
#  This section holds files that will increment your encoders. This is accomplished through the manual turning of a motor.
#  This code, count.py , must be run on applications like PUTTY and the corresponding motor must be manually turned by the user to collect and print data regarding the count.
#  To learn more and access this file, please see motor.Motor which is part of the \ref motor package.
#
#  @section sec_enc Encoder Driver
#  This section holds files that will extend interface which establishes a "handshake" between a Nucleo and a PC. This bidirectional communication is then used, here, for data collection.
#  This code will be split into 2 files - a Nucleo side, nucleo_side.py , and PC side, pc_side.py . The 'Nucleo side' must be run on applications like PUTTY while the 'PC side' must be run on applications like Spyder.
#  To learn more and access these files, please see encoder.Encoder which is part of the \ref encoder package.
#
#  @author Brooke Marvin
#
#  @copyright License Info
#
#  @date March 18, 2021
#