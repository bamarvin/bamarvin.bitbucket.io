'''@file    Implementing_FSMs.py
@brief      Code: https://bitbucket.org/bamarvin/me30502lab/src/master/Homework%20and%20Lab%202/Implementing_FSMs.py 
@details    Implements a finite state machine, shown below, to stimulate
            the behavior of an elevator.
@author     Brooke Marvin
@date       01/25/2021
@copyright  License Info Here
'''

import time
import random

def motor_cmd(cmd):
    '''@brief Commands the motor to move or stop
       @param cmd The command to give the motor
    '''
    if cmd=='UP':
        print('Mot up')
    elif cmd=='DOWN':
        print('Mot down')
    elif cmd=='STOP':
        print('Mot stop')


def T_sensor(): # Sensor on top (2nd) floor
    return random.choice([True, False]) # randomly return T or F

def B_sensor(): # Senesor on bottom (1st) floor
    return random.choice([True, False])

def go_button(): # Button senesor
    return random.choice([True, False])

# Main program / test program begin
#   This code only runs if the script is executed as main by pressing play
#   but does not run if the script is imported as a module
if __name__ == "__main__":
    # Program initialization goes here
    state = 0 # Initial state is the init state
    
    while True:
        try:
            if state==0:
                # run state 0 (init) code
                print('S0')
                motor_cmd('DOWN')
                state = 1 # Updating state for next iteration

            elif state==1:
                # run state 1 (moving down) code
                print('S1')
                # if go button is pressed, stop motor and transition to S2
                if B_sensor():
                    motor_cmd('STOP')
                    state = 2 
                    
            elif state==2:
                # run state 2 (stopped at first floor) code
                print('S2')
                print('button turned off')
                # if go button is pressed, start motor and transition to S3
                if go_button():
                    motor_cmd('UP')
                    state = 3 
                    
            elif state==3:
                # run state 1 (moving up) code
                print('S2')
                # If we are already at the first floor, stop motor and transition to S4
                if T_sensor():
                    motor_cmd('STOP')
                    state=4
     
            elif state==4:
                # run state 4 (stopped at second floor) code
                print('S4')
                print('button turned off')
                # if go button is pressed, start motor and transition to S1
                if go_button():
                    motor_cmd('DOWN')
                    state = 1 # Updating state for next iteration
                
            else:
                pass
            # code to run if state number is invalid
            # program should ideally never reach here
            
            # Slow down execution of FSM so we can see output in console
            time.sleep(0.2)
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the 
            # while(True) loop when desired
            print('Ctrl-c has been pressed')
            break
    
    # Program de-initialization goes here

