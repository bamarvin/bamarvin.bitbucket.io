## @file motor.py
#  Brief doc for motor.py
#
#  Detailed doc for motor.py 
#
#  @author Brooke Marvin
#
#  @copyright License Info
#
#  @date March 18, 2021
#
#  @package motor
#  Brief doc for the motor module
#
#  Detailed doc for the motor module
#
#  @author Brooke Marvin
#
#  @copyright License Info
#
#  @date March 18, 2021

import pyb

## A motor driver object
#
#  As the code is now, you can manually spin the motors and receive count readings labeled "CW" and "CCW." Further coding would have allowed a user to press 'z' to zero the encoder position, 'p' to print the encoder position, 'd' to print the encoder delta, 'g' to collect encoder data for 30 seconds, and 's' to end data collection.
#  @author Brooke Marvin
#  @copyright License Info
#  @date March 18, 2021
class Motor:

	## Constructor for motor driver
	#
	#  Detailed info on motor driver constructor
	def __init__(self):
		pass

	## Sets duty cycle for motor
	#
	#  Detailed info on motor driver duty cycle function
	#
	#  @param level The desired duty cycle
	def set_duty_cycle(self,level):
		pass