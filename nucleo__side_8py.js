var nucleo__side_8py =
[
    [ "data_collection_time", "nucleo__side_8py.html#acb9aa415678f330fe5787ee5af89e7be", null ],
    [ "flag", "nucleo__side_8py.html#a3b05a845c95463913c0acf0c3bf7a515", null ],
    [ "mytime", "nucleo__side_8py.html#ad9de2c71cc8c066ac8f27328ef0d6e89", null ],
    [ "myuart", "nucleo__side_8py.html#a5d3571ea3d2f0f4015e917cc079b30ee", null ],
    [ "n", "nucleo__side_8py.html#a1bfbac4a29310f97dc890c77ed1496c5", null ],
    [ "out_string", "nucleo__side_8py.html#a64dadaf62058207850dff48f366f0144", null ],
    [ "start_time_set", "nucleo__side_8py.html#a2e14fdcad1e9f23f7c01e0946151c131", null ],
    [ "state", "nucleo__side_8py.html#a79b07ce618bf40f33e16b1c532783389", null ],
    [ "time", "nucleo__side_8py.html#abaf8963a18c0d69f06cffccf67038e66", null ],
    [ "TIME", "nucleo__side_8py.html#a4893443369e37d76f3b58930b5a1389e", null ],
    [ "times", "nucleo__side_8py.html#a6fb05d324790f7a39d3e8935759e7be9", null ],
    [ "val", "nucleo__side_8py.html#a743d7a9652f1e519ac041d6691240bda", null ],
    [ "values", "nucleo__side_8py.html#a2c7bab94cac9fa6cda6b9e7b648aab00", null ],
    [ "x", "nucleo__side_8py.html#a5c4fbdd30c53c3046c50b9b87e311fd5", null ]
];