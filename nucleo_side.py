'''@file nucleo_side.py
Code: https://bitbucket.org/bamarvin/me30502lab/src/master/Final%20Project/nucleo_side.py

@details    This code establishes a handshake between a PC and a Nucleo. Pressing 'g' tells the Nucleo to start collecting data for 30 seconds. Pressing 's' tells the Nucleo to stop collecting data. The data that the Nucleo collects will be sent back to the PC. The PC will then plot this data. Furthermore, all collected values will be uploaded to a .csv file. This is the Nucleo side of the code (i.e. this file will be uploaded to the Nucleo and run on PUTTY). 
@author     Brooke Marvin
@date       03/18/2021
@copyright  License Info Here
'''


# Nucleo imports
#import pyb - does not need to be imported?
from pyb import UART 
import utime
from array import array
from math import exp, sin, pi


### Should serial only be on the PC???
#import serial # From example:
#ser = serial.Serial(port='COM4',baudrate=115273,timeout=5)

#def sendChar(): # Allows the PC to interact with the Nucleo (i.e. I can click keyboard buttons and the Nucleo will register them)
    #inv = input('Give me a character: ')
    #ser.write(str(inv).encode('ascii')) # PC is writing to the nucleo
    #myval = ser.readline().decode('ascii') # PC decodes and associates keyboard keys with number values
    #return myval

#for n in range(1):
    #print(sendChar())
    



if __name__ =='__main__':
    
    state = 0
    time = 0
    
    myuart = UART(2) # From example:

    
    times = array('f', 124*[0]) # Data points on plot ### Should I change this to 30???
    values = array('f', 124*[0])
    n = 0
    x = 0
    
    val = '' 
    start_time_set = False
    flag = False
    

    while True:
        try:
            if state == 0:
                #print('state 0')
                #print('Press g to start collecting data. Press s to stop collecting data. Press ctrl-c to end the program at any time. ')
                   
                if myuart.any() != 0: # From example:
                    val = myuart.readchar() # Nucleo is reading the character that the PC sent it (The keyboard keys appear to have numbers associated with them)
                    myuart.write('You sent an ASCII '+ str(val) +' to the Nucleo. ') # Nucleo writes this to the PC in response
                    
                    if val == 103:
                        #print('g has been pressed. Collecting data. This will take 30 seconds...')
                        start_time_set == False
                        flag = False                                                                    
                        state = 1 
                        
                    
                    elif val == 115:
                        #print('Data collection has not started')
                        state = 0
            
            if state == 1:
                #print('state 1')

                
                if start_time_set == False:
                    mytime = utime.ticks_ms() # State's timer
                    #print('Time slowly passes by...: ')
                    #print(mytime)
                    start_time_set = True

                if utime.ticks_diff(utime.ticks_ms(), mytime) <= 32000: # 30 seconds
                    data_collection_time = utime.ticks_diff(utime.ticks_ms(), mytime) # Timer (in miliseconds) for data collection

                    
                    if flag == False: 
                        flag = True
                        TIME = utime.ticks_ms() 

                    if flag == True:

                                               
                        if utime.ticks_diff(utime.ticks_ms(), TIME) >= 250: ### Hmmm, the time seems to be off by 2 seconds
                            #print('data_collection_time: ')
                            #print(data_collection_time/1000)
                            

                            
                            times[n] = x
                            #print('times: ')
                            #print(times)                           
                            
                            values[n] = exp((-x)/10)*sin(2*pi/3*(x)) #Adjusted to work with my timer
                            #print('values:')
                            #print(values[n])
                            

                            
                            #print('data: ')
                            #print('{:}, {:}'.format(times[n], values[n]))
                            
                            out_string = '{:}, {:}\r\n'.format(times[n], values[n])
                            myuart.write(out_string.encode())
                            
                            n += 1 # I think this is a counter
                            #print('n value')
                            #print(n)
                            
                            x += .25
                            #print('x value: ')
                            #print(x)

                            
                            flag = False
                        
                        
                   
                    # As an example, print the data as a CSV to the console
                    #for n in range(len(times)):

                        #print('{:}, {:}'.format(data_collection_time/1000, values[n]))
                        #print(str(x) + ', ' + str(values[n]))
                        
                        #out_string = '{:}, {:}'.format(x, values[n]) 
                        #myuart.write(out_string.encode())
                    


                    if myuart.any() != 0: # From example:
                        val = myuart.readchar() # Nucleo is reading the character that the PC sent it (The keyboard keys appear to have numbers associated with them)
                        myuart.write('You sent an ASCII '+ str(val) +' to the Nucleo. ') # Nucleo writes this to the PC in response
                        ## Use val == # since working with the Nucleo
                        if val == 115:
                            #print('Data collection stopped')
                            val = ''
                            state = 0
               
                    
                elif utime.ticks_diff(utime.ticks_ms(), mytime) > 32000: # 30 seconds
                    #print('All data has been collected')
                    #print('Come back again if you ever want to collect more data!') # The program is only letting me collect data once for some reason...?
                    
                    mytime = 0
                    data_collection_time = 0
                    val = ''
                    x = 0
                    n = 0
                    break
       
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the 
            # while(True) loop when desired
            print('Ctrl-c has been pressed. Program ending.')
            #ser.close()
            break
    
    # Program de-initialization goes here
            
            
            
            
            
            
            
            
            
            
            
            